import React from "react";

export const Footer = () => {
  return (
    <footer className="fixed bottom-0 left-0 z-20 w-full p-4 bg-white border-t border-slate-200 shadow p-6 dark:bg-slate-900 dark:border-slate-500">
      <p className="text-sm text-gray-500 sm:text-center dark:text-gray-400">
        © {new Date().getFullYear()}
        {/* TODO: Change link later after hosting on netlify */}
        <a href="https://bingefiesta.netlify.app" className="hover:text-primary-800">
          BingeFiesta&trade;
        </a>
        . All Rights Reserved.
      </p>
    </footer>
  );
};
