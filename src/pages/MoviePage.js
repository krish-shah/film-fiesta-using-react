import React, { useEffect } from 'react'
import { MovieCard, MovieCardSkeleton } from "../components"
import { useDynamicTitle, useFetch } from '../hooks/'

export const MoviePage = ({apiPath, title}) => {
  const BASE_API = process.env.REACT_APP_API_URL
  const {data:movies, isLoading, setUrl } = useFetch();

  useDynamicTitle(title);
  useEffect(() => {
    setUrl(`${BASE_API}${apiPath}?api_key=${process.env.REACT_APP_API_KEY}`)
  }, [BASE_API, apiPath, setUrl])

  function renderSkeletons(count) {
    const skeletons = [];
    for(let i=1; i <= count; i++)
      skeletons.push(<MovieCardSkeleton key={i}/>);
    return skeletons;
  }

  return (
    <main>
      <div className="flex flex-wrap justify-start">
        {
          isLoading && renderSkeletons(6)
        }
        {
          !isLoading && movies && movies.results.map(movie => <MovieCard movie={movie} key={movie.id}/>)
        }
      </div>
    </main>
  )
}
